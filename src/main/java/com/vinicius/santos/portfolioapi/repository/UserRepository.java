package com.vinicius.santos.portfolioapi.repository;

import java.util.UUID;

import com.vinicius.santos.portfolioapi.model.entity.UserEntity;

public interface UserRepository extends GenericRepository<UserEntity, UUID> {
    
}
