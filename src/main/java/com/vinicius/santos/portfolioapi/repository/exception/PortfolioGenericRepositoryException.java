package com.vinicius.santos.portfolioapi.repository.exception;

public class PortfolioGenericRepositoryException extends Exception {
    
    private static final long serialVersionUID = -4088210950784337141L;

    public PortfolioGenericRepositoryException() {

    }

    public PortfolioGenericRepositoryException(String message) {
        super(message);
    }

    public PortfolioGenericRepositoryException(String message, Throwable cause){
        super(message, cause);
    }

    public PortfolioGenericRepositoryException(Throwable cause){
        super(cause);
    }

    public PortfolioGenericRepositoryException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace){
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
