package com.vinicius.santos.portfolioapi.repository.impl;

import java.util.UUID;

import com.vinicius.santos.portfolioapi.model.entity.UserEntity;
import com.vinicius.santos.portfolioapi.repository.UserRepository;

import org.springframework.stereotype.Repository;

@Repository
public class UserRepositoryImpl extends GenericRepositoryImpl<UserEntity, UUID> implements UserRepository {
    
}
