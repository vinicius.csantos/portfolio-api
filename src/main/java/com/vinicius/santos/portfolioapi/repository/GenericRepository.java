package com.vinicius.santos.portfolioapi.repository;

import java.io.Serializable;
import java.util.List;

import com.vinicius.santos.portfolioapi.model.entity.Domain;

import com.vinicius.santos.portfolioapi.repository.exception.PortfolioGenericRepositoryException;

public interface GenericRepository<DomainClass extends Domain, Pk extends Serializable> {

    DomainClass saveOrUpdate(DomainClass object) throws PortfolioGenericRepositoryException;

    void delete(DomainClass object) throws PortfolioGenericRepositoryException;

    List<DomainClass> findAll() throws PortfolioGenericRepositoryException;
}
