package com.vinicius.santos.portfolioapi.repository.exception;

public class PortfolioExecutionException extends Exception{
    
    private static final long serialVersionUID = -6401074432135173652L;

    public PortfolioExecutionException() {

    }

    public PortfolioExecutionException(String message) {
        super(message);
    }

    public PortfolioExecutionException(String message, Throwable cause){
        super(message, cause);
    }

    public PortfolioExecutionException(Throwable cause){
        super(cause);
    }

    public PortfolioExecutionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace){
        super(message, cause, enableSuppression, writableStackTrace);
    }
    
}
