package com.vinicius.santos.portfolioapi.repository.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.validation.ConstraintViolationException;

import com.vinicius.santos.portfolioapi.model.entity.Domain;
import com.vinicius.santos.portfolioapi.repository.GenericRepository;
import com.vinicius.santos.portfolioapi.repository.exception.PortfolioGenericRepositoryException;

import org.hibernate.Criteria;
import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.metadata.ClassMetadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation = Propagation.SUPPORTS)
public class GenericRepositoryImpl<DomainClass extends Domain, Pk extends Serializable>
        implements GenericRepository<DomainClass, Pk> {

    @Autowired
    private EntityManager entityManager;

    private boolean defaultReadOnly;

    @SuppressWarnings("unchecked")
    private Class<Pk> getPersistenceClass() {
        return (Class<Pk>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @SuppressWarnings("deprecation")
    protected Criteria getCriteria() {

        return getSession().createCriteria(getPersistenceClass());
    }

    @SuppressWarnings("deprecation")
    public Session getSession() {

        this.defaultReadOnly = false;
        Session currentSession = entityManager.unwrap(Session.class);
        currentSession.setDefaultReadOnly(defaultReadOnly);

        if (defaultReadOnly) {
            currentSession.setFlushMode(FlushMode.MANUAL);
        } else {
            currentSession.setFlushMode(FlushMode.AUTO);
        }

        return currentSession;
    }
    public ClassMetadata getMetaData() {

        ClassMetadata currentSession = entityManager.unwrap(ClassMetadata.class);

        return currentSession;
    }

    @SuppressWarnings("unchecked")
    @Transactional
    public DomainClass saveOrUpdate(DomainClass domainInstance) throws PortfolioGenericRepositoryException {

        try {
            if (getSession().contains(domainInstance)) {
                Serializable identifier = getSession().getIdentifier(domainInstance);
                if (identifier == null) {
                    domainInstance = (DomainClass) getSession().save(domainInstance);
                } else {
                    domainInstance = (DomainClass) getSession().merge(domainInstance);
                }

            } else {
                domainInstance = (DomainClass) getSession().merge(domainInstance);
            }
            return domainInstance;
        } catch (Exception e) {
            throw new PortfolioGenericRepositoryException(e.getMessage(), e);
        }
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    public void delete(DomainClass domainInstance) throws PortfolioGenericRepositoryException {

        try {
            getSession().delete(domainInstance);

        } catch (ConstraintViolationException e) {
            throw new PortfolioGenericRepositoryException(e.getMessage(), e);
        } catch (HibernateException e) {
            throw new PortfolioGenericRepositoryException(e.getMessage(), e);
        } catch (DataIntegrityViolationException e) {
            throw new PortfolioGenericRepositoryException(e.getMessage(), e);
        } catch (RuntimeException e) {
            throw new PortfolioGenericRepositoryException(e.getMessage(), e);
        } catch (Throwable e) {
            throw new PortfolioGenericRepositoryException(e.getMessage(), e);
        }
    }

    @SuppressWarnings("unchecked")
    public DomainClass findById(Pk id) throws PortfolioGenericRepositoryException {

        try {
            DomainClass domainClass = (DomainClass) getSession().get(getPersistenceClass(), id);

            return domainClass;
        } catch (Exception e) {
            throw new PortfolioGenericRepositoryException(e.getMessage(), e);
        }
    }

    @SuppressWarnings("unchecked")
    public List<DomainClass> findAll() throws PortfolioGenericRepositoryException {

        try {
            return getCriteria().setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        } catch (Exception e) {
            throw new PortfolioGenericRepositoryException(e.getMessage(), e);
        }
    }

}
