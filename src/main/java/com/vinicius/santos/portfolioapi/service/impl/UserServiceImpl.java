package com.vinicius.santos.portfolioapi.service.impl;

import com.vinicius.santos.portfolioapi.model.entity.UserEntity;
import com.vinicius.santos.portfolioapi.model.to.UserView;
import com.vinicius.santos.portfolioapi.repository.UserRepository;
import com.vinicius.santos.portfolioapi.repository.exception.PortfolioExecutionException;
import com.vinicius.santos.portfolioapi.repository.exception.PortfolioGenericRepositoryException;
import com.vinicius.santos.portfolioapi.service.UserService;
import com.vinicius.santos.portfolioapi.util.UserUtil;
import com.vinicius.santos.portfolioapi.util.security.PasswordEncoder;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private static Logger logger = Logger.getLogger(UserServiceImpl.class);

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserUtil userUtil;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public UserView createUser(UserEntity userEntity) throws PortfolioExecutionException {
        try {
            String encodedPassword = passwordEncoder.bCryptPasswordEncoder().encode(userEntity.getUserPassword());
            userEntity.setUserPassword(encodedPassword);
            return userUtil.convertUserToUserView(userRepository.saveOrUpdate(userEntity));
        } catch (PortfolioGenericRepositoryException e) {
            logger.error(String.format("Fail to save user, username %s", userEntity.getUserUsername()));
            throw new PortfolioExecutionException("FAIL_SAVE_USER_REPOSITORY", e);
        }
    }

}
