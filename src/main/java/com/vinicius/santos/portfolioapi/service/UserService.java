package com.vinicius.santos.portfolioapi.service;

import com.vinicius.santos.portfolioapi.model.entity.UserEntity;
import com.vinicius.santos.portfolioapi.model.to.UserView;
import com.vinicius.santos.portfolioapi.repository.exception.PortfolioExecutionException;


public interface UserService {

    UserView createUser(UserEntity userEntity) throws PortfolioExecutionException;
    
}
