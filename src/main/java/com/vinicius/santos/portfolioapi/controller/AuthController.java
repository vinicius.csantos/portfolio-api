package com.vinicius.santos.portfolioapi.controller;

import com.vinicius.santos.portfolioapi.model.entity.UserEntity;
import com.vinicius.santos.portfolioapi.model.to.HttpResponse;
import com.vinicius.santos.portfolioapi.repository.exception.PortfolioExecutionException;
import com.vinicius.santos.portfolioapi.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/auth")
public class AuthController {

    @Autowired
    UserService userService;

    @PostMapping("/login")
    public HttpResponse logInUser() {
        return new HttpResponse(false, "Não foi possivel realizar o login do usuário");
    }

    @PostMapping("/create")
    public HttpResponse createUser(@RequestBody UserEntity userEntity) {
        try {
            return new HttpResponse(true, userService.createUser(userEntity));
        } catch (PortfolioExecutionException e) {
            return new HttpResponse(false, e.getMessage());
        }
    }

}
