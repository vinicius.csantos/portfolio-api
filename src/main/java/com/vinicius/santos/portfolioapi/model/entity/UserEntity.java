package com.vinicius.santos.portfolioapi.model.entity;

import java.sql.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;

import org.hibernate.annotations.GenericGenerator;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "user")
public class UserEntity implements Domain {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "userId", updatable = false, nullable = false)
    @ApiModelProperty(hidden = true)
    private UUID userId;

    @Column(name = "userName", nullable = false, unique = false)
    private String userName;

    @Email
    @Column(name = "userEmail", nullable = false, unique = true)
    private String userEmail;

    @Column(name = "userUsername", nullable = false, unique = true)
    private String userUsername;

    @Column(name = "userBirthDate", nullable = false, unique = false)
    private Date userBirthDate;

    @Column(name = "userCpfCnpj", nullable = false, unique = true)
    private String userCpfCnpj;

    @Column(name = "userPassword", nullable = false, unique = false)
    private String userPassword;

    public UUID getUserId() {
        return this.userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return this.userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserUsername() {
        return this.userUsername;
    }

    public void setUserUsername(String userUsername) {
        this.userUsername = userUsername;
    }

    public Date getUserBirthDate() {
        return this.userBirthDate;
    }

    public void setUserBirthDate(Date userBirthDate) {
        this.userBirthDate = userBirthDate;
    }

    public String getUserCpfCnpj() {
        return this.userCpfCnpj;
    }

    public void setUserCpfCnpj(String userCpfCnpj) {
        this.userCpfCnpj = userCpfCnpj;
    }

    public String getUserPassword() {
        return this.userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

}
