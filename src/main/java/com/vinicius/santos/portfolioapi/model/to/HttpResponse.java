package com.vinicius.santos.portfolioapi.model.to;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class HttpResponse implements Serializable {
    
    private static final long serialVersionUID = -8461561920577335100L;

    private boolean success;

    private Object object;

    private String message;

    public HttpResponse(boolean success){
        this.success = success;
    }

    public HttpResponse(boolean success, String message){
        this.success = success;
        this.message = message;
    }

    public HttpResponse(boolean success, Object object){
        this.success = success;
        this.object = object;
    }

    public boolean isSuccess() {
        return this.success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Object getObject() {
        return this.object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    


}
