package com.vinicius.santos.portfolioapi.model.to;

import java.sql.Date;
import java.util.UUID;

public class UserView {

    private UUID userId;

    private String userName;

    private String userEmail;

    private String userUsername;

    private Date userBirthDate;

    private String userCpfCnpj;

    public UUID getUserId() {
        return this.userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return this.userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserUsername() {
        return this.userUsername;
    }

    public void setUserUsername(String userUsername) {
        this.userUsername = userUsername;
    }

    public Date getUserBirthDate() {
        return this.userBirthDate;
    }

    public void setUserBirthDate(Date userBirthDate) {
        this.userBirthDate = userBirthDate;
    }

    public String getUserCpfCnpj() {
        return this.userCpfCnpj;
    }

    public void setUserCpfCnpj(String userCpfCnpj) {
        this.userCpfCnpj = userCpfCnpj;
    }
    
}
