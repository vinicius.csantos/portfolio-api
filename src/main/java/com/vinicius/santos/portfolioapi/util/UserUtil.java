package com.vinicius.santos.portfolioapi.util;

import com.vinicius.santos.portfolioapi.model.entity.UserEntity;
import com.vinicius.santos.portfolioapi.model.to.UserView;

import org.springframework.stereotype.Component;

@Component
public class UserUtil {

    public UserView convertUserToUserView(UserEntity userEntity){
        UserView userView = new UserView();
        userView.setUserBirthDate(userEntity.getUserBirthDate());
        userView.setUserCpfCnpj(userEntity.getUserCpfCnpj());
        userView.setUserEmail(userEntity.getUserEmail());
        userView.setUserId(userEntity.getUserId());
        userView.setUserName(userEntity.getUserName());
        userView.setUserUsername(userEntity.getUserUsername());

        return userView;
    }
    
}
